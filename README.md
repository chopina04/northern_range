# northern_range

Jeux de données géomatiques pour enseigner la Northern Range en cours de Géographie.

Sources des données :

<a href="http://ec.europa.eu/eurostat/data/database">Eurostat</a><br>
<a href="https://esa.un.org/Unpd/Wup/Download/">The 2018 Revision of World Urbanization Prospects</a><br>
<a href="http://www.lboro.ac.uk/gawc/world2016t.html">The World According to GaWC 2016</a><br>
<a href="http://lmr.havre-port.net/">HAROPA - Le Havre</a><br>
<a href="http://www.aapa-ports.org/unifying/content.aspx?ItemNumber=21048">America Association of Port Authorities</a><br>

Source des couches vectorielles :<br>

<a href="http://ec.europa.eu/eurostat/fr/web/gisco/geodata/reference-data">Eurostat - Gisco</a><br>
<a href="http://overpass-turbo.eu/">Données osm</a><br>

